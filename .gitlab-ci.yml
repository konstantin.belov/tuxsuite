include:
  - https://gitlab.com/Linaro/tuxpkg/-/raw/main/gitlab-ci-pipeline.yml

variables:
  TUXPKG_PROJECT: tuxsuite

.job:
  script:
  - make $CI_JOB_NAME

stylecheck:
  image: $CI_REGISTRY_IMAGE:ci-debian
  stage: test
  script:
    - python3 -m pip install --break-system-packages -r requirements-dev.txt
    - flake8 .
    - black --check --diff .

test-markdown:
  stage: test
  image: docker.io/pipelinecomponents/markdownlint
  script:
    - scripts/markdownlint

wheel:
  image: $CI_REGISTRY_IMAGE:ci-debian
  stage: build
  script:
    - flit build
  artifacts:
    paths:
      - dist/*.whl

.docker:
  services:
    - name: docker:dind
  image: docker
  only:
    - tags
  before_script:
    - docker info
    - docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
    - export tag="tuxsuite/tuxsuite:${CI_COMMIT_TAG##v}${TAG_APPEND}"
    - export latest="tuxsuite/tuxsuite:latest${TAG_APPEND}"
  script:
    - docker build --pull --tag "${tag}" --tag "${latest}" .
    - docker run "${tag}" tuxsuite --version
    - docker run "${latest}" tuxsuite --version
    - docker push "${tag}"
    - docker push "${latest}"
  needs:
    - wheel

docker-amd64:
  extends: .docker
  stage: deploy
  variables:
    TAG_APPEND: -amd64

docker-arm64:
  extends: .docker
  stage: deploy
  tags:
     - arm64-dind
  variables:
    TAG_APPEND: -arm64

publish-dockerhub:
  extends: .docker
  stage: .post
  variables:
    DOCKER_CLI_EXPERIMENTAL: "enabled"
  script:
    - docker manifest create "${tag}" "${tag}-amd64" "${tag}-arm64"
    - docker manifest create "${latest}" "${latest}-amd64" "${latest}-arm64"
    - docker manifest push "${tag}"
    - docker manifest push "${latest}"
  needs:
    - docker-amd64
    - docker-arm64

doc:
  extends: .job
  image: $CI_REGISTRY_IMAGE:ci-debian
  stage: build
  artifacts:
    paths:
      - public
  before_script:
    - python3 -m pip install --break-system-packages mkdocs-material mkdocs-mermaid2-plugin

pages:
  extends: .job
  image: $CI_REGISTRY_IMAGE:ci-debian
  stage: deploy
  needs:
    - doc
    - repository
  artifacts:
    paths:
      - public
  only:
    - tags
  script:
    - cp -r dist/repo public/packages/

.tuxsuite:
  except:
    - schedules

.public-ecr:
  services:
    - name: docker:dind
  image: docker
  only:
    - tags
  before_script:
    - apk add --no-cache python3 py3-pip
    - pip3 install --no-cache-dir awscli
    - docker info
    - aws --region $AWS_DEFAULT_REGION ecr-public get-login-password | docker login --username AWS --password-stdin "${PUBLIC_ECR}"
    - export tag="${PUBLIC_ECR}:${CI_COMMIT_TAG##v}${TAG_APPEND}"
    - export latest="${PUBLIC_ECR}:latest${TAG_APPEND}"
  script:
    - docker build --pull --tag "${tag}" --tag "${latest}" .
    - docker run "${tag}" tuxsuite --version
    - docker run "${latest}" tuxsuite --version
    - docker push "${tag}"
    - docker push "${latest}"
  needs:
    - wheel

public-ecr-amd64:
  extends: .public-ecr
  stage: deploy
  variables:
    TAG_APPEND: -amd64

public-ecr-arm64:
  extends: .public-ecr
  stage: deploy
  tags:
     - arm64-dind
  variables:
    TAG_APPEND: -arm64

publish-public-ecr:
  extends: .public-ecr
  stage: .post
  variables:
    DOCKER_CLI_EXPERIMENTAL: "enabled"
  script:
    - docker manifest create "${tag}" "${tag}-amd64" "${tag}-arm64"
    - docker manifest create "${latest}" "${latest}-amd64" "${latest}-arm64"
    - docker manifest push "${tag}"
    - docker manifest push "${latest}"
  needs:
    - public-ecr-amd64
    - public-ecr-arm64
