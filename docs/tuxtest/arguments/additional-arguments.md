# Additional Arguments

## callback

`--callback` is an optional argument which POSTs JSON data that has
the status of the test, at the end of the test to the given URL. The
URL should be a valid http(s) link that accepts POST data.

[See Callbacks Reference, for more details](../../callbacks.md)
